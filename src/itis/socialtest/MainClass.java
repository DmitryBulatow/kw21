package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("src\\itis\\socialtest\\resources\\PostDatabase.csv",
		        "src\\itis\\socialtest\\resources\\Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
	    File authorsFile = new File(authorsSourcePath);
	    File postsFile = new File(postsSourcePath);
		List<String> authors = new ArrayList<>();
		List<String> posts = new ArrayList<>();

	    try (BufferedReader brAuthors = new BufferedReader(
					    new FileReader(authorsFile));
	        BufferedReader brPosts = new BufferedReader(
			        new FileReader(postsFile))
	    ) {
		    String s = brAuthors.readLine();
		    while (s!=null){
				authors.add(s);
				s = brAuthors.readLine();
			}
		    Set<Author> authorSet = parseAuthor(authors);
		    s = brPosts.readLine();
			while (s!=null){
				posts.add(s);
				s = brPosts.readLine();
			}
		    Set<Post> postsSet = parsePost(posts, authorSet);

			AnalyticsService analyticsService = new AnalyticsServiceImpl();

			firstQuestion(postsSet);
		    System.out.println(analyticsService.findPostsByDate(postsSet.stream().toList(), "17.04.2021"));
		    System.out.println(analyticsService.findMostPopularAuthorNickname(postsSet.stream().toList()));
		    System.out.println(analyticsService.checkPostsThatContainsSearchString(postsSet.stream().toList(), "Россия"));
		    System.out.println(analyticsService.findMostPopularAuthorNickname(postsSet.stream().toList()));
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
    }

	private void firstQuestion(Set<Post> postsSet) {
		postsSet.forEach(System.out::println);
	}


	private Set<Author> parseAuthor(List<String> authors) {
		Set<Author> authorSet = new HashSet<>();
		for (String author : authors) {
			String[] split = author.split(", ");
			authorSet.add(new Author(Long.parseLong(split[0]), split[1], split[2]));
		}
		return authorSet;
	}

	private Set<Post> parsePost(List<String> posts, Set<Author> authorSet) {
		Set<Post> postSet = new HashSet<>();
		for (String post : posts) {
			String[] split = post.split(", ");
			postSet.add(new Post(split[2].split("T")[0], split[3], Long.valueOf(split[1]),
						authorSet.stream()
								.filter(author -> author.getId() == Long.valueOf(split[0]))
								.findFirst()
								.get()
								));
		}
		return postSet;
	}
}
